from jira import JIRA
# from jira.client import ResultList
# from jira.resources import Issue
# jira = JIRA(server="https://jira.atlassian.com")

server = input("Enter your jira/atlassian link: ")
jiraOptions = {'server': server}
email = input("Enter your email address: ")
pat = input("Enter your api token: ")
jira = JIRA(
    options=jiraOptions,
    basic_auth=(email, pat),  # a username/password tuple [Not recommended]
    # basic_auth=("email", "API token"),  # Jira Cloud: a username/token tuple
    # token_auth="API token",  # Self-Hosted Jira (e.g. Server): the PAT token
    # auth=("admin", "admin"),  # a username/password tuple for cookie auth [Not recommended]
)
# print(jira)
# print(jira.search_issues(jql_str='project = LEAR'))

for singleIssue in jira.search_issues(jql_str='project = LEAR'):
    print(f"{singleIssue.key}: {singleIssue.fields.summary} by {singleIssue.fields.reporter.displayName}")
